#/usr/bin/env bash

source ./env
echo sbt.version=$SBT_VERSION > fake_proj/project/build.properties
echo "scalaVersion := \"$SCALA_VERSION\"" > fake_proj/build.sbt

echo "Using $CI_REGISTRY_IMAGE:$TAG"
docker build --build-arg SCALA_VERSION=$SCALA_VERSION --build-arg SBT_VERSION=$SBT_VERSION --build-arg JAVA_VERSION=$JAVA_VERSION -t $CI_REGISTRY_IMAGE:${BUILD_TAG} .

