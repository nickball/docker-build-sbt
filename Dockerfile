ARG JAVA_VERSION
FROM openjdk:$JAVA_VERSION-jdk-stretch

ARG SBT_VERSION
ENV SBT_VERSION=$SBT_VERSION
ARG SCALA_VERSION
ENV SCALA_VERSION=$SCALA_VERSION

RUN apt-get update -yqq && apt-get install apt-transport-https -yqq
RUN echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && apt-get update -yqq && apt-get install sbt=$SBT_VERSION -yqq && rm -rf /var/lib/apt/lists/*

COPY fake_proj /tmp/fake_proj
RUN cd /tmp/fake_proj && sbt compile clean && cd / && rm -rf /tmp/fake_proj
